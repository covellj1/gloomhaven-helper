import Nav from "./(components)/Nav";

export default function Home() {
  return (
    <div className="bg-gray-100 h-screen">
      <Nav />
      <main className="container mx-auto py-8">
        <h2 className="text-2xl font-bold mb-4">About Gloomhaven</h2>
        <p>
          Gloomhaven is a cooperative board game for 1-4 players designed by
          Isaac Childres and published by Cephalofair Games in 2017. It is set
          in a persistent world of shifting motives, changing environments, and
          evolving characters.
        </p>
        <p>
          Players take on the role of wandering mercenaries with their own
          special set of skills and reasons for traveling to this dark corner of
          the world. As players explore the world, they will face challenges,
          make decisions, and uncover secrets.
        </p>
      </main>
      <footer className="bg-gray-800 text-white py-4 text-center fixed bottom-0 w-full">
        <p>&copy; 2024 Gloomhaven Board Game</p>
      </footer>
    </div>
  );
}
