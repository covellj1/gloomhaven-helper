export default function Nav() {
  return (
    <>
      <header className="bg-gray-800 text-white py-4">
        <h1 className="text-3xl text-center font-bold">Gloomhaven</h1>
      </header>
      <nav className="bg-gray-700 text-white py-2">
        <ul className="flex justify-center">
          <li className="mx-4">
            <a href="#" className="hover:text-gray-300">
              Home
            </a>
          </li>
          <li className="mx-4">
            <a href="#" className="hover:text-gray-300">
              About
            </a>
          </li>
          <li className="mx-4">
            <a href="#" className="hover:text-gray-300">
              Characters
            </a>
          </li>
          <li className="mx-4">
            <a href="#" className="hover:text-gray-300">
              Scenarios
            </a>
          </li>
          <li className="mx-4">
            <a href="#" className="hover:text-gray-300">
              Shop
            </a>
          </li>
        </ul>
      </nav>
    </>
  );
}
